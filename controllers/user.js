const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');

//Check Email
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length >0) {
			return true 
		} else {
			return false
		}
	})
}

//Register
module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		firstName:reqBody.firstName,
		lastName:reqBody.lastName,
		email:reqBody.email,
		mobileNo:reqBody.mobileNo,
		password:bcrypt.hashSync(reqBody.password,10)
		//Syntax: bcrypt.hashSync(<stringToBeHashed>, <saltRounds>) 
		//salt Rounds - no. of time siya i-eencrypt

	})

	return newUser.save().then((user,error)=>{
		if(error){
			return false
		} else {
			return true
		}
	})

}

//Login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result =>{

		if(result == null){
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			} else{
				return false
			}
		}
	})
}

//Activity 

module.exports.getProfile = (reqBody) => {

	return User.findById({_id:reqBody._id}).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.password = " "
		return result.save().then((updatedPassword, err) => {

			if(err){
				return false
			} else {

				return updatedPassword
			}
		})
	})
}